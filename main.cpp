#include <iostream>
#include <iomanip>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include<cmath>
#include<fstream>
using namespace std;

/*Definicja funkcji obliczaj�cej wektor prawych stron uk�adu r�wna�*/
int funcX (double t, const double y[], double f[], void *params)
{
    (void)(t); /* avoid unused parameter warning */
    double mu = *(double *)params; //to jest nasze k/m

    double Ux=*((double *)params+1);
    double Uy=*((double *)params+2);
    double Uz=*((double *)params+3);
    double Wx=*((double *)params+4);
    double Wy=*((double *)params+5);
    double Wz=*((double *)params+6);
    //cout<<"TO JEST WX "<<Wx<<endl;
    f[0] = y[1];
    f[1] = -mu*sqrt((y[1]-Wx)*(y[1]-Wx)+(Uy-Wy)*(Uy-Wy)+(Uz-Wz)*(Uz-Wz))*(y[1]-Wx);

    return GSL_SUCCESS;
}

int funcY (double t, const double y[], double f[], void *params)
{
    (void)(t); /* avoid unused parameter warning */
    double mu = *(double *)params;

    double Ux=*((double *)params+1);
    double Uy=*((double *)params+2);
    double Uz=*((double *)params+3);
    double Wx=*((double *)params+4);
    double Wy=*((double *)params+5);
    double Wz=*((double *)params+6);

    f[0] = y[1];
    f[1] = -mu*sqrt((Ux-Wx)*(Ux-Wx)+(y[1]-Wy)*(y[1]-Wy)+(Uz-Wz)*(Uz-Wz))*(y[1]-Wy);

    return GSL_SUCCESS;
}

int funcZ (double t, const double y[], double f[], void *params)
{
    (void)(t); /* avoid unused parameter warning */
    double mu = *(double *)params;

    double Ux=*((double *)params+1);
    double Uy=*((double *)params+2);
    double Uz=*((double *)params+3);
    double Wx=*((double *)params+4);
    double Wy=*((double *)params+5);
    double Wz=*((double *)params+6);

    f[0] = y[1];
    f[1] = -9.81-mu*sqrt((Ux-Wx)*(Ux-Wx)+(Uy-Wy)*(Uy-Wy)+(y[1]-Wz)*(y[1]-Wz))*(y[1]-Wz);

    return GSL_SUCCESS;
}
/*Definicja funkcji obliczaj�cej macierz Jacobiego*/
int jacX (double t, const double y[], double *dfdy, double dfdt[], void *params)
{
    (void)(t); /* avoid unused parameter warning */

    double mu=*((double *)params);
    double Ux=*((double *)params+1);
    double Uy=*((double *)params+2);
    double Uz=*((double *)params+3);
    double Wx=*((double *)params+4);
    double Wy=*((double *)params+5);
    double Wz=*((double *)params+6);

    gsl_matrix_view dfdy_mat= gsl_matrix_view_array (dfdy, 2, 2);
    gsl_matrix * m = &dfdy_mat.matrix;
    gsl_matrix_set (m, 0, 0, 0.0);
    gsl_matrix_set (m, 0, 1, 1.0);
    gsl_matrix_set (m, 1, 0, -mu*(Uy-Wy)*(Uy-Wx)/sqrt((Ux-Wx)*(Ux-Wx)+(Uy-Wy)*(Uy-Wy))-mu*sqrt((Ux-Wx)*(Ux-Wx)+(Uy-Wy)*(Uy-Wy)));
    gsl_matrix_set (m, 1, 1, 0);
    dfdt[0] = 0.0;
    dfdt[1] = 0.0;

    return GSL_SUCCESS;
}
/*Definicja funkcji obliczaj�cej macierz Jacobiego*/
int jacY (double t, const double y[], double *dfdy, double dfdt[], void *params)
{
    (void)(t); /* avoid unused parameter warning */

    double mu=*((double *)params);
    double Ux=*((double *)params+1);
    double Uy=*((double *)params+2);
    double Uz=*((double *)params+3);
    double Wx=*((double *)params+4);
    double Wy=*((double *)params+5);
    double Wz=*((double *)params+6);

    gsl_matrix_view dfdy_mat= gsl_matrix_view_array (dfdy, 2, 2);
    gsl_matrix * m = &dfdy_mat.matrix;
    gsl_matrix_set (m, 0, 0, 0.0);
    gsl_matrix_set (m, 0, 1, 1.0);
    gsl_matrix_set (m, 1, 0, -mu*(Uy-Wy)*(Uy-Wy)/sqrt((Ux-Wx)*(Ux-Wx)+(Uy-Wy)*(Uy-Wy))-mu*sqrt((Ux-Wx)*(Ux-Wx)+(Uy-Wy)*(Uy-Wy)));
    gsl_matrix_set (m, 1, 1, 0);
    dfdt[0] = 0.0;
    dfdt[1] = 0.0;

    return GSL_SUCCESS;
}
/*Definicja funkcji obliczaj�cej macierz Jacobiego*/
int jacZ (double t, const double y[], double *dfdy, double dfdt[], void *params)
{
    (void)(t); /* avoid unused parameter warning */

    double mu=*((double *)params);
    double Ux=*((double *)params+1);
    double Uy=*((double *)params+2);
    double Uz=*((double *)params+3);
    double Wx=*((double *)params+4);
    double Wy=*((double *)params+5);
    double Wz=*((double *)params+6);

    gsl_matrix_view dfdy_mat= gsl_matrix_view_array (dfdy, 2, 2);
    gsl_matrix * m = &dfdy_mat.matrix;
    gsl_matrix_set (m, 0, 0, 0.0);
    gsl_matrix_set (m, 0, 1, 1.0);
    gsl_matrix_set (m, 1, 0, -mu*(Uy-Wy)*(Uy-Wy)/sqrt((Ux-Wx)*(Ux-Wx)+(Uy-Wy)*(Uy-Wy))-mu*sqrt((Ux-Wx)*(Ux-Wx)+(Uy-Wy)*(Uy-Wy)));
    gsl_matrix_set (m, 1, 1, 0);
    dfdt[0] = 0.0;
    dfdt[1] = 0.0;

    return GSL_SUCCESS;
}

int main (void)
{
    double h,U,Wx,Wy,Wz,Phi,m,k;// zmienne tymczasowe - do podstawienia przez wybrane przez piszacych funkcje

    cout << "Witaj w symulatorze trakcji ruchu obiektu wyrzuconego w powietrze " << endl;
    cout << "Symulator potrzebuje: " << endl;

    cout << " - wysokosci poczatkowej h[m] :" << endl << "h=";
    cin >> h;

    cout <<" - predkosci poczatkowej U[m/s]:" << endl << "U=";
    cin >> U;

    cout <<" - kata wzgledem osi x pod jakim wyrzucono cialo w powietrze Phi[deg]:" << endl << "phi=";
    cin >> Phi;

    cout <<" - skladowych predkosci poczatkowej wiatru W[m/s]:" << endl << "Wx=";
    cin >> Wx;
    cout << "Wy";
    cin >> Wy;
    cout << "W";z
    cin >> Wz;

    cout <<" - masy ciala m[kg]:" << endl << "m=";
    cin >> m;

	cout <<" - stalej oporu powietrza (dodatniej) k[kg/m]:" << endl << "k=";
   // do{ cin >> k;}
   // while(k<0);

        Phi=Phi*0.017453;
        alfa=alfa*0.017453;

        k=1.471/m;

        double Ux=U*cos(Phi);
        double Uy=0.0;
        double Uz=U*sin(Phi);

        double par[7] = {k, Ux, Uy, Uz, Wx, Wy, Wz};
        double t = 0.0;
        double x[2] = { 0, U*cos(Phi)};
        double y[2] = {0,0};
        double z[2] = { h, U*sin(Phi)};

            gsl_odeiv2_system sysX = {funcX, jacX, 2, &par};
            gsl_odeiv2_driver * X = gsl_odeiv2_driver_alloc_y_new(&sysX, gsl_odeiv2_step_rk8pd,1e-6, 1e-6, 0.0);

            gsl_odeiv2_system sysY = {funcY, jacY, 2, &par};
            gsl_odeiv2_driver * Y = gsl_odeiv2_driver_alloc_y_new(&sysY, gsl_odeiv2_step_rk8pd,1e-6, 1e-6, 0.0);

            gsl_odeiv2_system sysZ = {funcZ, jacZ, 2, &par};
            gsl_odeiv2_driver * Z = gsl_odeiv2_driver_alloc_y_new(&sysZ, gsl_odeiv2_step_rk8pd,1e-6, 1e-6, 0.0);

    fstream czas;
    fstream x0;
    fstream x1;
    fstream y0;
    fstream y1;
    fstream z0;
    fstream z1;


        czas.open( "czas.txt", ios::out );
        if( !czas.good() == true )
        {
           cout << "Blad otwarcia pliku" << endl;
        }
        x0.open( "x[0].txt", ios::out );
        if( !x0.good() == true )
        {
           cout << "Blad otwarcia pliku" << endl;
        }
        x1.open( "x[1].txt", ios::out );
        if( !x1.good() == true )
        {
           cout << "Blad otwarcia pliku" << endl;
        }
        y0.open( "y[0].txt", ios::out );
        if( !y0.good() == true )
        {
           cout << "Blad otwarcia pliku" << endl;
        }
        y1.open( "y[1].txt", ios::out );
        if( !y1.good() == true )
        {
           cout << "Blad otwarcia pliku" << endl;
        }
        z0.open( "z[0].txt", ios::out );
        if( !z0.good() == true )
        {
           cout << "Blad otwarcia pliku" << endl;
        }
        z1.open( "z[1].txt", ios::out );
        if( !z1.good() == true )
        {
           cout << "Blad otwarcia pliku" << endl;
        }



    for (int i = 1; i <= 1000; i++)
    {
        int s = gsl_odeiv2_driver_apply_fixed_step(X,&t,0.0005,100,x);
        par[1]=x[1];

        if(s != GSL_SUCCESS)
        {
            cout << "Blad obliczen" << endl;
            break;
        }

        s = gsl_odeiv2_driver_apply_fixed_step(Y,&t,0.0005,100,y);
        par[2]=y[1];

        if(s != GSL_SUCCESS)
        {
            cout << "Blad obliczen" << endl;
            break;
        }

        s = gsl_odeiv2_driver_apply_fixed_step(Z,&t,0.0005,100,z);
        par[3]=z[1];

        if(s != GSL_SUCCESS)
        {
            cout << "Blad obliczen" << endl;
            break;
        }



        if(z[0]<=0) break;

        czas<<t<<"\n";
        x0<<x[0]<<"\n";
        x1<<x[1]<<"\n";
        y0<<y[0]<<"\n";
        y1<<y[1]<<"\n";
        z0<<z[0]<<"\n";
        z1<<z[1]<<"\n";
    }

    gsl_odeiv2_driver_free (X);
    gsl_odeiv2_driver_free (Y);
    gsl_odeiv2_driver_free (Z);
}

